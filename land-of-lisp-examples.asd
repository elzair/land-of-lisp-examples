;;;; -*- mode: lisp; indent-tabs-mode: nil -*-
(defpackage #:land-of-lisp-examples
  (:use :cl :asdf))

(in-package #:land-of-lisp-examples)

(asdf:defsystem :land-of-lisp-examples
  :version "0.1.0"
  :author "Conrad Barski"
  :maintainer "Philip Woods <elzairthesorcerer@gmail.com>"
  :components
  ((:static-file "README.md")
   (:file "chapter06")
   (:file "chapter065")
   (:file "chapter14")
   (:module "dod"
            :depends-on ("lazy")
            :components
            ((:file "dod_v1")
             (:file "dod_v2" :depends-on ("dod_v1" "lazy"))))
   (:module "evolve"
            :components
            ((:file "evolve")))
   (:module "lazy"
            :components
            ((:file "lazy")
             (:file "list" :depends-on ("lazy"))))
   (:module "orc"
            :components
            ((:file "battle")))
   (:module "svg"
            :depends-on ("util")
            :components
            ((:file "svg" :depends-on ("macros"))))
   (:module "util"
            :components
            ((:file "graph-util")
             (:file "macros")))
   (:module "wizard"
            :components
            ((:file "advanced" :depends-on ("adventure"))
             (:file "adventure")))
   (:module "wumpus"
            :depends-on ("util")
            :components
            ((:static-file "city")
             (:file "wumpus" :depends-on ("graph-util"))))
   (:module "www"
            :components
            ((:file "server")))))
