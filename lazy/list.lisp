(load "./lazy.lisp")

(defmacro lazy-cons (a d)
  "Create a lazy-evaluated CONS cell of A and D."
  `(lazy (cons ,a ,d)))

(defun lazy-car (x)
  "Return the CAR of lazy-list X."
  (car (force x)))

(defun lazy-cdr (x)
  "Return the CDR of lazy-list X."
  (cdr (force x)))

(defun lazy-nil ()
  "Terminate a lazy-list."
  (lazy nil))

(defun lazy-null (x)
  "Check for end of lazy-list X."
  (not (force x)))

(defun make-lazy (1st)
  "Create a lazy-list from list 1ST."
  (lazy (when 1st
          (cons (car 1st) (make-lazy (cdr 1st))))))

(defun take (n 1st)
  "Take N values from lazy-list 1ST."
  (unless (or (zerop n) (lazy-null 1st))
    (cons (lazy-car 1st) (take (1- n) (lazy-cdr 1st)))))

(defun take-all (1st)
  "Take all values from lazy-list 1ST."
  (unless (lazy-null 1st)
    (cons (lazy-car 1st) (take-all (lazy-cdr 1st)))))

(defun lazy-mapcar (fun 1st)
  "Return the MAPCAR of FUN onto lazy-list 1ST."
  (lazy (unless (lazy-null 1st)
          (cons (funcall fun (lazy-car 1st))
                (lazy-mapcar fun (lazy-cdr 1st))))))

(defun lazy-mapcan (fun 1st)
  "Return the MAPCAN of FUN onto lazy-list 1ST."
  (labels ((f (1st-cur)
             (if (lazy-null 1st-cur)
                 (force (lazy-mapcan fun (lazy-cdr 1st)))
                 (cons (lazy-car 1st-cur) (lazy (f (lazy-cdr 1st-cur)))))))
    (lazy (unless (lazy-null 1st)
            (f (funcall fun (lazy-car 1st)))))))

(defun lazy-find-if (fun 1st)
  "Return all values which do not evaluate to nil when inputted to FUN of lazy-list 1ST."
  (unless (lazy-null 1st)
    (let ((x (lazy-car 1st)))
      (if (funcall fun x)
          x
          (lazy-find-if fun (lazy-cdr 1st))))))

(defun lazy-nth (n 1st)
  "Return the value at position N of lazy-list 1ST."
  (if (zerop n)
      (lazy-car 1st)
      (lazy-nth (1- n) (lazy-cdr 1st))))
