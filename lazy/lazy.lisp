(defmacro lazy (&body body)
  "Create a lazy-evaluated function from BODY."
  (let ((forced (gensym))
        (value (gensym)))
    `(let ((,forced nil)
           (,value nil))
       (lambda ()
         (unless ,forced
           (setf ,value (progn ,@body))
           (setf ,forced t))
         ,value))))

(defun force (lazy-value)
  "Force the evaluation of a LAZY-VALUE."
  (funcall lazy-value))
